import numpy as np
from pylab import *
import os, json, sys
from scipy.spatial.distance import cdist
from scipy.stats import rankdata
from copy import deepcopy
from sklearn.neighbors import NearestNeighbors
import networkx as nx

X = np.load('features.npy')
y = np.load('labels.npy')
print X.shape, y.shape

Xsim = cdist(X, X, 'minkowski', 1)#[:5,:5]
X_rank = []
for x_i in range(len(Xsim)):
    X_rank.append(rankdata(Xsim[x_i], 'ordinal').tolist())
Xrank = (np.array(X_rank)-1)

# Also find a nearest neighbor matrix:
K = 50
# Q: what to compute nearest neighbors on - makes sense to use rank!!
# can do Xsim too?
nbrs = NearestNeighbors(n_neighbors = K+1 ).fit(Xrank)
# since NN also includes itself - elimintae it by looking up K+1 and removing th e first
Knn_idxs = nbrs.kneighbors(Xrank)[1][:, 1:]

def cluster_level_dist(Xsim_full, ab_list):
    Xsim = np.empty((len(ab_list), len(ab_list)))
    for a_ix, a_ in enumerate(ab_list):
        for b_ix, b_ in enumerate(ab_list):
            Xsim_zip = Xsim_full[np.ix_(a_,b_)]
            a_idx,b_idx = np.unravel_index(np.argmin(Xsim_zip), Xsim_zip.shape)
            Xsim[a_ix, b_ix] = Xsim_full[a_[a_idx],b_[b_idx]]

    X_rank = []
    for x_i in range(len(Xsim)):
        X_rank.append(rankdata(Xsim[x_i], 'ordinal').tolist())
    Xrank = (np.array(X_rank) - 1)
    return Xsim, Xrank


# asymmetrical rank order dist (A_ROD) -
#  Zhu et al. "A Rank-Order Distance based Clustering Algorithm for Face Tagging", CVPR 2011
#  Corresponds to eq. 1, or D(a,b)
# Xrank- rank matrix
# K = parameter indicating nerest neighbors
def A_ROD(Xsim, Xrank, a_, b_, Knn_idxs=None, K=10):
    # K = Knn_idxs.shape[]

    #make nearest neigh idxs if None
    if Knn_idxs is None:
        nbrs = NearestNeighbors(n_neighbors = K+1 ).fit(Xrank)
        Knn_idxs = nbrs.kneighbors(Xrank)[1][:, 1:]

    if len(a_)==1 and len(b_)==1:
        a = a_[0]
        b = b_[0]
    else:
        Xsim_zip = Xsim[np.ix_(a_,b_)]
        a_idx,b_idx = np.unravel_index(np.argmin(Xsim_zip), Xsim_zip.shape)
        a = a_[a_idx]
        b = b_[b_idx]

    d_a_b = Xsim[a, b]
    Oa_b = Xrank[a, b]

    #also do Ob_a which you can use for normalization
    Ob_a = Xrank[b,a]
    d_norm_factor = float(min(Oa_b, Ob_a))

    # LIMIT_RANK = False
    # L=100
    # # print Oa_b, '---'
    # # print Oa_b
    # # here add a condition to limit # neighbors (k) - say if Oa_b<20 (here k=20)
    # if LIMIT_RANK:
    #     Oa_b = min(L, Oa_b)

    # reference paper:
    # https://pdfs.semanticscholar.org/efd6/4b7641bea8ca536f4e179be6e2dd25d519d6.pdf
    #eqn 3 - rank-order clustering
    d_r_ab_list = []
    for i in range(0,int(Oa_b)+1):
        d_i = Xrank[b,np.where(Xrank[a,:] == i)[0][0]]
        d_r_ab_list.append(d_i)
    d_r_ab = np.nan_to_num(sum(d_r_ab_list)/d_norm_factor)

    #eqn. 5 - phi__j?
    all_ab = a_ + b_
    phi_i_j = sum([ sum(Xsim[i, Knn_idxs[i,:]])/K for i in all_ab ])
    phi_i_j /= float(len(all_ab))
    #eqn 5
    d_n_ab = d_a_b/phi_i_j

    # # Eqn 3 - clustering millions of faces by identity paper
    # d_m_ab = 0
    # for i in range(1,min(100,int(Oa_b))):
    #     d_i = Xrank[b,np.where(Xrank[a,:] == i)[0][0]]
    #     if d_i not in Knn_idxs[b,:]: d_m_ab+=1

    return d_r_ab, d_n_ab#, d_m_ab

def Approx_A_ROD(Xsim, Xrank, a_, b_, Knn_idxs=None, K=50):
    # K = Knn_idxs.shape[]

    #make nearest neigh idxs if None
    if Knn_idxs is None:
        nbrs = NearestNeighbors(n_neighbors = K+1 ).fit(Xrank)
        Knn_idxs = nbrs.kneighbors(Xrank)[1][:, 1:]

    if len(a_)==1 and len(b_)==1:
        a = a_[0]
        b = b_[0]
    else:
        Xsim_zip = Xsim[np.ix_(a_,b_)]
        a_idx,b_idx = np.unravel_index(np.argmin(Xsim_zip), Xsim_zip.shape)
        a = a_[a_idx]
        b = b_[b_idx]

    d_a_b = Xsim[a, b]
    Oa_b = Xrank[a, b]

    #also do Ob_a which you can use for normalization
    Ob_a = Xrank[b,a]
    d_norm_factor = float(min(Oa_b, Ob_a))

    # Eqn 3 - clustering millions of faces by identity paper
    d_m_ab = 0
    for i in range(1,min(K,int(Oa_b))):
        d_i = Xrank[b,np.where(Xrank[a,:] == i)[0][0]]
        if d_i in Knn_idxs[b,:]: d_m_ab+=1
    d_m_ab /= d_norm_factor

    return d_m_ab

D_r_ab = np.zeros(Xrank.shape)
D_N_ab = np.zeros(Xrank.shape)
D_m_ab = np.zeros(Xrank.shape)
for a in range(len(D_r_ab)):
    for b in range(len(D_r_ab)):
        # if not a%100: print a,b
        if a!=b:
            D_r_ab[a,b], D_N_ab[a,b] = A_ROD(Xsim, Xrank, [a], [b], K=10, Knn_idxs=Knn_idxs)
            D_m_ab[a,b] = \
                Approx_A_ROD(Xsim, Xrank, [a], [b], K=50, Knn_idxs=Knn_idxs)

#symmetrizing A_ROD - D_ab =  DR_ab
D_R_ab = np.nan_to_num( (D_r_ab + D_r_ab.T) )


D_M_ab = np.nan_to_num( (D_m_ab + D_m_ab.T))
                        # / np.minimum(Xrank, Xrank.T).astype('float') )
# LIMIT_RANK = False
# if LIMIT_RANK:
#     L = 100
#     D_R_ab = np.nan_to_num((D_r_ab + D_r_ab.T).astype('float'))


def find_rank_order_distances(Xsim, Xrank):
    D_r_ab = np.zeros(Xsim.shape)
    D_N_ab = np.zeros(Xsim.shape)
    K=10
    if Xsim.shape[0]<=K: K = min(K, Xsim.shape[0])-1
    nbrs = NearestNeighbors(n_neighbors=K + 1).fit(Xrank)
    Knn_idxs = nbrs.kneighbors(Xrank)[1][:, 1:]

    # D_m_ab = np.empty_like(Xrank)
    for a in range(len(D_r_ab)):
        for b in range(len(D_r_ab)):
            # if not a%100: print a,b
            if a!=b:
                D_r_ab[a, b], D_N_ab[a, b] = A_ROD(Xsim, Xrank, [a], [b], Knn_idxs=Knn_idxs)
            # if not a%100 and not b%100: print a,b
    # symmetrizing A_ROD - D_ab =  DR_ab
    D_R_ab = np.nan_to_num((D_r_ab + D_r_ab.T))
    return D_R_ab, D_N_ab

from sklearn.metrics import silhouette_score
# clustering process here - rank order distance face tagging Algo. 1



def transitive_clustering(t, C_0):
    C_new = deepcopy(C_0)
    keep_merging = 2
    ITER = 0
    cluster_idxs_dict = {}
    cluster_idxs_dict[ITER] = C_new
    while (keep_merging>1):
        ITER += 1
        Xsim_new, Xrank_new = cluster_level_dist(Xsim, C_new)
        D_R_new, D_N_new = find_rank_order_distances(Xsim_new, Xrank_new)
        merge_status = (D_R_new<t)*(D_N_new<1)
        G = nx.from_numpy_matrix(merge_status)
        C_1 = []
        nodes_to_merge = [i.nodes() for i in nx.connected_component_subgraphs(G)]
        for i in nodes_to_merge:
            c_1 = []
            for j in i: c_1+=C_new[j]
            C_1.append(c_1)
        C_new = C_1
        cluster_idxs_dict[ITER] = C_new
        keep_merging = max([len(i) for i in nodes_to_merge])
        print 'thresh = ', t,' ; # clusters', keep_merging, len(C_new)

    return cluster_idxs_dict

# C_0 = []#[[i] for i in range(len(Xrank))]
# t = 4
# figure out which cells to merge
# merge_status_0 = (D_R_ab<t)*(D_N_ab<1)
# G_0 = nx.from_numpy_matrix(merge_status_0)
# for i in nx.connected_component_subgraphs(G_0): C_0.append(i.nodes())

ALL_cluster_idxs = []
for t in [2,4,6,8,10,15,20,25,30,35,40,45,50]:
    C_0 = []
    merge_status_0 = (D_R_ab < t) * (D_N_ab < 1)
    G_0 = nx.from_numpy_matrix(merge_status_0)
    for i in nx.connected_component_subgraphs(G_0): C_0.append(i.nodes())

    c_idx_dict = transitive_clustering(t, C_0)
    ALL_cluster_idxs.append(c_idx_dict)

